terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "ap-south-1"
}

#EC2 Instnace

resource "aws_instance" "demo" {
  ami           = "ami-076e3a557efe1aa9c"
  instance_type = "t2.micro"  
  tags = {
    Name = "demo-test"
   } 
}

